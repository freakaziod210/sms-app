I used the React CLI to create this project.

To get going create a token.js file in the /src directory.
Here is an example of how I did it: 
```
const token = "your-token-here";

export default token;
```
