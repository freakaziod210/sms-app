import React, { Component } from 'react';
import './App.css';
import DataService from '../data-service';
import token from '../token.js';
import ContactsList from '../components/contacts-list/contacts-list.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
    }
  }

  componentDidMount() {
    DataService.getContactsList(token, 1, 3).then((result) => {
      this.setState({
        contacts: result.data.data
      })
    })
  }

  sendSms = (token, contactId, message) => {
    console.log('sms', contactId, token, message);
    DataService.sendMessage(token, contactId, message);
  }

  render() {
    console.log('state', this.state);
    return (
      <div className="App">
        <h1 className="title">Contacts List</h1>
          { this.state.contacts.map((val, idx, arr) => {
              return (
                <ContactsList
                  name={ val.full_name }
                  id={ val.id }
                  avatar={ val.avatar_url }
                  key={idx}
                  onClick={(id, message) => this.sendSms(token, id, message)}
                />
              )
            })
          }
      </div>
    );
  }
}

export default App;
