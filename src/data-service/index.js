import axios from 'axios';

export default class DataService {
  static getContactsList(token, page, per) {
    return axios.get(`https://stage.skipio.com/api/v2/contacts?token=${token}&page=${page}&per=${per}`)
  }

  static sendMessage(token, id, message) {
    console.log('message', message);
    return axios({
      method: 'POST',
      url: `https://stage.skipio.com/api/v2/messages?token=${token}`,
      header: 'content-type: application/json',
      data: {
        "recipients": [
          `contact-${id}`
        ],
        "message": {
          "body": message
        }
      }
    });
  }
}
