import React, { Component } from 'react';
import './contacts-list.css';
import '@material/button/dist/mdc.button.min.css';

export default class ContactsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textarea: '',
      showInput: false,
    }
    this.handleIconClick.bind(this);
  }

  handleChange(event) {
    console.log(event.target.value);
    this.setState({ textarea: event.target.value });
  }

  handleIconClick() {
    console.log('setting showInput')
    const shouldInputShow = this.state.showInput ? false : true;
    this.setState({
      showInput: shouldInputShow
    });
  }

  render() {
    return (
      <div className="contact-list-component">
        <div className="list-wrapper">
          <div className="contact-wrapper">
            <img className="contact-avatar" src={this.props.avatar} alt="avatar"/>
            <div className="contact-name">{this.props.name}</div>
          </div>
          <div className="icon-wrapper">
            <i className="material-icons icons phone">phone</i>
            <i className="material-icons icons chat" onClick={() => this.handleIconClick() }>chat</i>
          </div>
        </div>
        { this.state.showInput ? this.Input() : null }
      </div>
    )
  }

  Input () {
    return (
      <div className="input-wrapper">
        <textarea className="textarea" name="textarea" placeholder="Message" value={this.state.textarea} maxLength="140" rows="3" cols="50" onChange={this.handleChange.bind(this)}></textarea>
        <button className="mdc-button mdc-button--raised" onClick={(e) => {
          this.props.onClick(this.props.id, this.state.textarea);
          this.setState({ textarea: ''});
        }}>Send</button>
      </div>
    )
  }
}
